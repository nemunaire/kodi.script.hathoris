#!/usr/bin/env python
import xbmc
import xbmcgui
import xbmcaddon
import json
import urllib.request
import logging

# Initialize the logger
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

ADDON = xbmcaddon.Addon()
ADDONID = xbmcaddon.Addon().getAddonInfo('id')
CWD = xbmcaddon.Addon().getAddonInfo('path')
API_URL = ADDON.getSetting('api_url')

CANCEL_DIALOG = (9, 10, 92, 216, 247, 257, 275, 61467, 61448)

def send_request(endpoint, data=None):
    url = f"{API_URL}{endpoint}"
    if data is not None:
        data = json.dumps(data).encode('utf-8')
    req = urllib.request.Request(url, data, headers={'Content-Type': 'application/json'})
    response = urllib.request.urlopen(req)
    if response.status > 300:
         res = json.loads(response.read().decode('utf-8'))
         xbmcgui.Dialog().notification('Error', f"Failed to create dialog: {res['errmsg']}", xbmcgui.NOTIFICATION_ERROR)
         raise Exception(res["errmsg"])
    return json.loads(response.read().decode('utf-8'))


# Sources ##########################################################

def get_sources():
    sources = send_request("/sources")
    res = []
    for src in sources:
        source = sources[src]
        source["id"] = src
        res.append(source)
    return res
    
def get_current_source():
    sources = send_request("/sources")
    for src in sources:
        source = sources[src]
        if source["enabled"]:
            return sources[src]["name"]
    return None

def enable_source(src):
    send_request("/sources/" + src + "/enable", {})
    return True


# Mixer ############################################################

def get_mixers():
    return send_request("/mixer")

def mixer_value(mixer):
    if mixer["Type"] == "ENUMERATED":
        return str(mixer["items"][mixer["values"][0]])
    elif mixer["Type"] == "BOOLEAN":
        return "Oui" if mixer["items"][mixer["values"][0]] else "Non"
    elif mixer["Type"] == "INTEGER" and "DBScale" in mixer:
        return "%0.1f dB" % (mixer["DBScale"]["Min"] + mixer["DBScale"]["Step"] * mixer["values"][0])
    else:
        return str(mixer["values"][0])

def set_mixer(mixer, value):
    val = []
    for v in mixer["values"]:
        val.append(value)
    send_request("/mixer/%s/values" % mixer["NumID"], val)
    return True


class AmplifierControlDialog(xbmcgui.WindowXMLDialog):
    def __init__(self, *args, **kwargs):
        super(AmplifierControlDialog, self).__init__(*args, **kwargs)
        self.data = kwargs['optional1']
    
    def onInit(self):
        self.getControl(2).setLabel("Hathoris")

        self.list = self.getControl(10)
        self.list.reset()
        self.list.addItem(xbmcgui.ListItem("Source", get_current_source()))

        self.mixers = get_mixers()
        for mixer in self.mixers:
            self.list.addItem(xbmcgui.ListItem(mixer["Name"], mixer_value(mixer)))

        #self.getControl(100).setLabel("Volume")
        #self.getControl(200).setLabel("Source")
        return

    def onClick(self, controlId):
        if controlId == 10:
            if self.list.getSelectedPosition() == 0:
                sources = get_sources()
                selected = xbmcgui.Dialog().select('Sélectionnez la source à utiliser', [s["name"] for s in sources])
                if selected != -1:
                    enable_source(sources[selected]["id"])
                    self.onInit()
            elif self.mixers[self.list.getSelectedPosition() - 1]["Type"] == "ENUMERATED":
                items = self.mixers[self.list.getSelectedPosition() - 1]["items"]
                selected = xbmcgui.Dialog().select(self.mixers[self.list.getSelectedPosition() - 1]["Name"], items, preselect=self.mixers[self.list.getSelectedPosition() - 1]["values"][0])
                if selected != -1:
                    set_mixer(self.mixers[self.list.getSelectedPosition() - 1], selected)
                    self.onInit()
            elif self.mixers[self.list.getSelectedPosition() - 1]["Type"] == "BOOLEAN":
                items = ["Non", "Oui"]
                selected = xbmcgui.Dialog().select(self.mixers[self.list.getSelectedPosition() - 1]["Name"], items, preselect=1 if self.mixers[self.list.getSelectedPosition() - 1]["values"][0] else 0)
                if selected != -1:
                    set_mixer(self.mixers[self.list.getSelectedPosition() - 1], True if selected else False)
                    self.onInit()
            else:
                newval = xbmcgui.Dialog().numeric(0, self.mixers[self.list.getSelectedPosition() - 1]["Name"], str(self.mixers[self.list.getSelectedPosition() - 1]["values"][0]))
                set_mixer(self.mixers[self.list.getSelectedPosition() - 1], int(newval))
                self.onInit()
        elif controlId == 100:
            volume = self.getControl(101).getValue()
            send_request('set_volume', {'volume': volume})
        elif controlId == 200:
            source = self.getControl(201).getSelectedItem().getLabel()
            send_request('set_source', {'source': source})
        elif controlId == 18:
            self.close()

    def onAction(self, action):
        if action.getButtonCode() in CANCEL_DIALOG:
            self.close()

if __name__ == '__main__':
    try:
      dialog = AmplifierControlDialog('amplifier_control.xml', CWD, 'default', optional1='some data')
      dialog.doModal()
      del dialog
    except Exception as e:
        logger.error(f"Failed to create dialog: {e}")
        xbmcgui.Dialog().notification('Error', f"Failed to create dialog: {e}", xbmcgui.NOTIFICATION_ERROR)
