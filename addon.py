#!/usr/bin/env python
import xbmc
import xbmcgui
import xbmcaddon
import json
import urllib.request

# Load the settings
ADDON = xbmcaddon.Addon()
API_URL = ADDON.getSetting('api_url')

def send_request(endpoint, data):
    url = f"{API_URL}/{endpoint}"
    data = json.dumps(data).encode('utf-8')
    req = urllib.request.Request(url, data, headers={'Content-Type': 'application/json'})
    response = urllib.request.urlopen(req)
    return json.loads(response.read().decode('utf-8'))

class AmplifierControlDialog(xbmcgui.WindowXMLDialog):
    def __init__(self, *args, **kwargs):
        super(AmplifierControlDialog, self).__init__(*args, **kwargs)
    
    def onInit(self):
        #self.getControl(100).setLabel("Volume")
        #self.getControl(200).setLabel("Source")
        pass

    def onClick(self, controlId):
        if controlId == 100:
            volume = self.getControl(101).getValue()
            send_request('set_volume', {'volume': volume})
        elif controlId == 200:
            source = self.getControl(201).getSelectedItem().getLabel()
            send_request('set_source', {'source': source})

if __name__ == '__main__':
    dialog = AmplifierControlDialog('amplifier_control.xml', ADDON.getAddonInfo('path'), 'default', '720p')
    dialog.doModal()
    del dialog
